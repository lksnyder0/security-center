$(document).ready( function() {
    $("ul.nav-sidebar").find("a").each(function () {
        if ( $(this).attr('href') == window.location.pathname) {
            $(this).addClass("active");
        }
    });
    $(".alert-message").alert();
    window.setTimeout(function() { $(".alert-message").alert('close'); }, 5000);
    $(".sidebar-expand").on("click", function () {
        $(".sidebar-wrapper").width(220);
    });
    $(".sidebar-collapse").on("click", function () {
        $(".sidebar-wrapper").width(0);
    });
    $('[data-toggle="tooltip"]').tooltip();
} );

addalert = function (message, level) {
    alertdiv = $("#alerts");
    alertcontent = '<div class="row">' +
        '<div class="alert alert-dismissible alert-' + level + ' alert-message fade in" role="alerts">' +
        message +
        '</div>' +
        '</div>';
    alertdiv.append(alertcontent);
    $(".alert-message").alert();
    window.setTimeout(function() { $(".alert-message").alert('close'); }, 5000);
};

