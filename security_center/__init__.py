# -*- coding: utf-8 -*-
# Security-Center (c) Luke Snyder

import logging
from flask import Flask, redirect, send_from_directory, url_for
from flask_security import Security, MongoEngineUserDatastore, current_user
from flask_security import url_for_security
from flask_mail import Mail
from flask_assets import Environment, Bundle
from flask_admin import Admin
from flask_admin.menu import MenuLink

from .models.mongo import mongodb, User, Role
from .views.administration.forms import ExtendedRegistrationForm
from .views.administration.forms import ExtendedConfirmRegistrationForm
from .marshmallow import ma
from .views.administration.views import UserView, HiddenIndexView

# Setup App and DB connections
app = Flask(__name__)
app.config.from_envvar("SETTINGS")
mongodb.init_app(app)
ma.init_app(app)

# Setup Logging
handler = logging.FileHandler(app.config['LOG'])
handler.setFormatter(
    logging.Formatter('%(asctime)s %(levelname)s: %(message)s'))
app.logger.addHandler(handler)
if app.config.get("LOG_LEVEL") == "DEBUG":
    app.logger.setLevel(logging.DEBUG)
elif app.config.get("LOG_LEVEL") == "WARN":
    app.logger.setLevel(logging.WARN)
else:
    app.logger.setLevel(logging.INFO)
app.logger.info('Startup with log: %s' % app.config['LOG'])

# Setup Flask-Security
user_datastore = MongoEngineUserDatastore(mongodb, User, Role)
security = Security(app,
                    user_datastore,
                    register_form=ExtendedRegistrationForm,
                    confirm_register_form=ExtendedConfirmRegistrationForm)

# Setup Flask-Mail
mail = Mail(app)

# Setup Flask-Admin
admin = Admin(
        app,
        name=app.config["PROJECT_NAME"],
        index_view=HiddenIndexView(),
        template_mode="bootstrap3"
        )
admin.add_link(MenuLink(name="Exit", url="/"))
admin.add_view(UserView(User))

# Setup Blueprints
# administration blueprint is custom to this application
# from .views.administration import adminbase_blueprint
# app.register_blueprint(adminbase_blueprint, url_prefix="/admin")

# App Center Blueprint
from .views.app_center import appcenter_blueprint
app.register_blueprint(appcenter_blueprint)

# Setup Webassets
assets = Environment(app)

# Base Assets
fontawesome_css = Bundle(
        "assets/font-awesome/less/variables.less",
        "assets/font-awesome/less/font-awesome.less",
        filters="less"
        )
global_js = Bundle(
        "assets/jquery/dist/jquery.js",
        "assets/html.sortable/dist/html.sortable.js",
        "assets/tether/dist/js/tether.js",
        "assets/bootstrap/dist/js/bootstrap.js",
        "assets/select2/dist/js/select2.full.js",
        "assets/moment/moment.js",
        "assets/datatables/media/js/jquery.dataTables.js",
        "assets/datatables/media/js/dataTables.bootstrap4.js",
        "assets/spectrum/spectrum.js",
        "site/global.js",
        filters="jsmin",
        output="global.min.js"
        )
global_css = Bundle(
        "assets/tether/dist/css/tether.css",
        "assets/tether/dist/css/tether-theme-arrows.css",
        "assets/bootstrap/dist/css/bootstrap.css",
        "assets/font-awesome/css/font-awesome.css",
        "assets/select2/dist/css/select2.css",
        "assets/datatables/media/css/jquery.dataTables.css",
        "assets/datatables/media/css/dataTables.bootstrap4.css",
        "assets/spectrum/spectrum.css",
        "site/global.css",
        filters="cssmin",
        output="global.min.css"
        )

# App_center assets
appcenter_js = Bundle(
        "app_center/js/appcenter.js",
        filters="jsmin", output="appcenter.min.js"
        )
appcenter_css = Bundle(
        'app_center/css/appcenter.css',
        filters='cssmin', output="appcenter.min.css"
        )

assets.register('global_js', global_js)
assets.register('global_css', global_css)
assets.register('appcenter_js', appcenter_js)
assets.register('appcenter_css', appcenter_css)


# Setup Handlers
@app.errorhandler(403)
def page_forbidden(e):
    if current_user.is_authenticated:
        return redirect(url_for("admin.index"))
    else:
        return redirect(url_for_security("login"))


@app.route('/fonts/<path:path>')
def serve_fonts(path):
    return send_from_directory('static/assets/font-awesome/fonts', path)
