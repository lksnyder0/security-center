from flask import Blueprint

adminbase_blueprint = Blueprint('adminbase', __name__,
                                template_folder='templates',
                                static_folder='static')
