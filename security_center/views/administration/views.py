# -*- coding: utf-8 -*-
# Security-Center (c) Luke Snyder

from flask import redirect, url_for
from flask_admin import AdminIndexView, expose
from flask_admin.form import SecureForm
from flask_admin.contrib.mongoengine import ModelView
from flask_security import current_user
from flask_security.utils import encrypt_password
from wtforms import PasswordField
from wtforms.validators import DataRequired, Optional, Email, Length


class SecureModelView(ModelView):
    form_base_class = SecureForm

    def is_accessible(self):
        if current_user.is_active and current_user.has_role('Admin'):
            return True
        return False


class HiddenIndexView(AdminIndexView):
    def is_visible(self):
        return False

    def is_accessible(self):
        return current_user.is_active and current_user.has_role('Admin')

    @expose("/")
    def index(self):
        return redirect(url_for('index'))


class UserView(SecureModelView):
    column_exclude_list = ('password', 'active', 'confirmed_at')
    column_searchable_list = ('email', )
    can_view_details = True
    column_details_exclude_list = ('password')
    can_delete = False
    form_args = {
            'name': {
                'validators': [DataRequired(), Length(max=255)]
                },
            'email': {
                'validators': [DataRequired(), Email()]
                },
            'roles': {
                'validators': [DataRequired()]
                },
            'password': {
                'validators': [Optional()]
                },
            'active': {
                'validators': [Optional()]
                }
            }
    form_excluded_columns = [
            'confirmed_at',
            'last_login_at',
            'current_login_at',
            'last_login_ip',
            'current_login_ip',
            'login_count'
            ]
    form_ajax_ref = {
            'roles': {
                'fields': ['name', 'description']
                }
            }
    form_overrides = {
            "password": PasswordField
            }
    form_create_rules = ('name', 'email', 'password', 'roles')

    def create_model(self, form):
        self.model.register(
                name=form.name.data,
                email=form.email.data,
                password=form.password.data,
                confirmed=True,
                roles=[r.name for r in form.roles.data]
                )
        return redirect(url_for("user.index_view"))

    def on_model_change(form, model, is_created):
        print("on_model_change")
        print(form)

    def update_model(self, form, model):
        print(form.active.data)
        newdata = {
                "name": form.name.data,
                "email": form.email.data,
                "roles": form.roles.data,
                "active": form.active.data
                }
        if form.password.data != "":
            newdata["password"] = encrypt_password(form.password.data)
        print(newdata)
        model.update(**newdata)
        return True
