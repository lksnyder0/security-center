import datetime
from flask import current_app
from flask_security import UserMixin, RoleMixin
from flask_security.utils import encrypt_password
from security_center.models.mongo import mongodb


class Role(mongodb.Document, RoleMixin):
    name = mongodb.StringField(max_length=80, unique=True)
    description = mongodb.StringField(max_length=4096)

    def __str__(self):
        return self.name


class User(mongodb.Document, UserMixin):
    name = mongodb.StringField(max_length=255)
    email = mongodb.StringField(max_length=255, unique=True)
    password = mongodb.StringField(max_length=255, required=True)
    active = mongodb.BooleanField()
    confirmed_at = mongodb.DateTimeField()
    last_login_at = mongodb.DateTimeField()
    current_login_at = mongodb.DateTimeField()
    last_login_ip = mongodb.StringField(max_length=255)
    current_login_ip = mongodb.StringField(max_length=255)
    login_count = mongodb.IntField(default=0)
    roles = mongodb.ListField(mongodb.ReferenceField(Role), default=[])

    def __str__(self):
        return self.email

    def add_role(self, role_name):
        """
        update a User account so that it includes a new Role

        :param role_name: the name of the Role to add
        :type role_name: string
        """

        from security_center import user_datastore

        new_role = user_datastore.find_or_create_role(role_name)
        user_datastore.add_role_to_user(self, new_role)

    def confirm(self):
        self.update(
                confirmed_at=datetime.datetime.now(),
                active=True
                )

    @classmethod
    def register(cls, name, email, password, confirmed=False, roles=None):
        """
        Create a new user account.

        :param name: the name of the account
        :type name: string
        :param email: the email address used to identify the account
        :type email: string
        :param password: the plaintext password for the account
        :type password: string
        :param confirmed: whether to confirm the account immediately
        :type confirmed: boolean
        :param roles: a list containing the names of the Roles for this User
        :type roles: list(string)
        """

        from security_center import user_datastore

        new_user = user_datastore.create_user(
            name=name,
            email=email,
            password=encrypt_password(password)
        )
        if confirmed:
            new_user.confirm()
        if roles:
            for role_name in roles:
                new_user.add_role(role_name)
        current_app.logger.debug("Created user {0}".format(email))
        return new_user

    @classmethod
    def add_guest_user(cls,
                       name="guest",
                       email="guest@example.com",
                       password="guest"):
        cls.register(
            name=name,
            email=email,
            password=password,
            confirmed=True,
            roles=["User"]
        )

    @classmethod
    def add_admin_user(cls,
                       name="admin",
                       email="admin@example.com",
                       password="aaa"):
        cls.register(
            name=name,
            email=email,
            password=password,
            confirmed=True,
            roles=["Admin"]
        )
