from flask_security.forms import RegisterForm, ConfirmRegisterForm
from wtforms import TextField
from wtforms.validators import DataRequired, Length


class ExtendedRegistrationForm(RegisterForm):
    name = TextField("Name", validators=[DataRequired("Name is required"),
                                         Length(max=255)])


class ExtendedConfirmRegistrationForm(ConfirmRegisterForm):
    name = TextField("Name", validators=[DataRequired("Name is required"),
                                         Length(max=255)])
