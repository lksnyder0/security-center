import json
import magic
import re

from . import appcenter_blueprint
from .models import *
from .forms import *
from flask import render_template, redirect, flash, url_for, request
from flask import current_app, send_from_directory
from flask_security import login_required, roles_required
from mongoengine import Q
from mongoengine.errors import NotUniqueError, OperationError
from werkzeug.utils import secure_filename
from os import path, remove

acceptPattern = "^JPEG|^PNG"
rootDir = path.abspath(path.dirname(__file__))
uploadDir = path.join(rootDir, "static/uploads/")


# Setup Jinja2 variables
@appcenter_blueprint.context_processor
def inject_variables():
    categories = Category.objects.all().order_by('+weight')
    searchform = SearchForm()
    return dict(categories=categories, searchform=searchform)


@appcenter_blueprint.route("/")
@login_required
def index():
    firstCat = Category.objects.order_by("+weight").first()
    return redirect(url_for(".getApps", id=firstCat.id))


@appcenter_blueprint.route("/uploads/<path:path>")
@login_required
def uploads(path):
    return send_from_directory(uploadDir, path)


@appcenter_blueprint.route("/search/<terms>")
@login_required
def search(terms):
    response = {"apps": [], "docs": []}
    response["terms"] = terms
    apps = App.objects(
        Q(name__icontains=terms) | Q(description__icontains=terms))
    docs = Document.objects(
        Q(name__icontains=terms) | Q(description__icontains=terms))
    for each in apps:
        response["apps"].append(dict(name=each.name,
                                     description=each.description,
                                     link=each.link))
    for each in docs:
        response["docs"].append(dict(name=each.name,
                                     description=each.description,
                                     link=each.link))
    current_app.logger.debug(
        "Type:Response, Request:{0} {1}, Response:{2}".format(
            request.method, request.full_path, str(response)))
    return json.dumps(response)


@appcenter_blueprint.route("/cat/<id>")
@login_required
def getApps(id):
    currentcat = Category.objects.get_or_404(id=id)
    apps = App.objects(category_id=id).order_by("+name").all()
    categories = Category.objects.order_by("+name").all().order_by("+weight")
    tags = Tag.objects.order_by("+name").all().order_by("+name")
    addeditappform = AddEditAppForm()
    addeditappform.category.default = id
    addeditappform.process()
    addeditappform.category.choices = [(str(c.id), c.name) for c in categories]
    addeditappform.tags.choices = [(str(t.id), t.name) for t in tags]
    return render_template(
        "apps.html", addeditdocument=AddEditDocumentForm(),
        addeditappform=addeditappform, deleteappform=DeleteAppForm(),
        apps=apps, currentcat=currentcat)


@appcenter_blueprint.route("/app/edit/<id>", methods=["GET", "POST"])
@appcenter_blueprint.route("/app/new", defaults={'id': None}, methods=["POST"])
@login_required
@roles_required("Admin")
def appAddEdit(id):
    if id:
        app = App.objects.get_or_404(id=id)
    categories = Category.objects.order_by("+name").all()
    tags = Tag.objects.order_by("+name").all()
    addeditappform = AddEditAppForm()
    addeditappform.category.choices = [(str(c.id), c.name) for c in categories]
    addeditappform.tags.choices = [(str(t.id), t.name) for t in tags]
    response = {"messages": []}
    if addeditappform.validate_on_submit():
        response["status"] = "success"
        category = Category.objects.get(id=addeditappform.category.data)
        tags = [Tag.objects.get(id=t) for t in addeditappform.tags.data]
        try:
            filepath = None
            fileurl = None
            if addeditappform.icon.data:
                with magic.Magic() as m:
                    fileType = m.id_buffer(addeditappform.icon.data.read(100))
                    match = re.match(acceptPattern, fileType)
                    if match:
                        addeditappform.icon.data.stream.seek(0)
                        filename = secure_filename(
                            addeditappform.icon.data.filename)
                        filepath = path.join(uploadDir, filename)
                        fileurl = url_for('.uploads', path=filename)
                        addeditappform.icon.data.save(filepath)
                        current_app.logger.debug(
                            "Type:FileUpload, Status:Success, Filename:{0},\
                            Filetype:{1}, Path:{2}, Url:{3}".format(
                                filename, fileType, filepath, fileurl))
                    else:
                        current_app.logger.debug(
                            "Type:FileUpload, Status:Failure, Filename:{0},\
                            Filetype:{1}".format(
                                addeditappform.icon.data.filename, fileType))
                        raise TypeError("Incorrect file type")
            if id:
                app.update(
                        name=addeditappform.name.data,
                        link=addeditappform.link.data,
                        description=addeditappform.description.data,
                        category_id=category,
                        tags=tags
                        )
                if filepath:
                    if app.iconpath:
                        remove(app.iconpath)
                    app.update(
                            iconpath=filepath,
                            iconurl=fileurl
                            )
                flash("App successfully updated", "success")
            else:
                app = App(
                        name=addeditappform.name.data,
                        description=addeditappform.description.data,
                        link=addeditappform.link.data,
                        category_id=category,
                        tags=tags,
                        iconpath=filepath,
                        iconurl=fileurl
                        ).save()
                flash("App successfully added", "success")
        except NotUniqueError:
            response["status"] = "error"
            response["messages"].append(
                {"name": addeditappform.name.name,
                 "error": "Name already used"})
        except TypeError:
            response["status"] = "error"
            response["messages"].append(
                {"name": addeditappform.icon.name,
                 "error": "File must be JPG or PNG"})
        else:
            response["messages"].append(
                {"action": "redirect",
                 "location": url_for('.getApps', id=category.id)})
    elif request.method == "POST":
        response["status"] = "error"
        for field in addeditappform:
            for error in field.errors:
                response["messages"].append(
                    {"name": field.name, "error": error})
    else:
        response = dict(
            name=app.name,
            description=app.description,
            link=app.link,
            category=str(app.category_id.id),
            tags=[str(t.id) for t in app.tags]
            )
    current_app.logger.debug(
        "Type:Response, Request:POST {0}, Response:{1}".format(
            url_for('.appAddEdit'), str(response)))
    return json.dumps(response)


@appcenter_blueprint.route("/app/delete/<id>", methods=["POST"])
@login_required
def appDelete(id):
    app = App.objects.get_or_404(id=id)
    catid = str(app.category_id.id)
    app.delete()
    flash("App deleted", "success")
    return redirect(url_for('.getApps', id=catid))


@appcenter_blueprint.route("/documents/<id>")
@login_required
def getDocumentation(id):
    documents = Document.objects(app_id=id).all()
    app = App.objects.get(id=id)
    return render_template('docs_list.html', id=id, catid=app.category_id.id,
                           documents=documents)


@appcenter_blueprint.route("/manage")
@login_required
@roles_required("Admin")
def manageIndex():
    return render_template("manage/index.html")


@appcenter_blueprint.route("/manage/documentation")
@login_required
@roles_required("Admin")
def manageDocuments():
    documents = Document.objects.all().limit(500)
    return render_template("manage/documentation.html", documents=documents)


@appcenter_blueprint.route("/manage/documentation/new", defaults={"id": None},
                           methods=["POST", "GET"])
@appcenter_blueprint.route("/manage/documentation/<id>",
                           methods=["POST", "GET"])
@login_required
@roles_required("Admin")
def manageDocumentsNewEdit(id):
    apps = App.objects.all().order_by("+Name")
    addeditdocumentform = AddEditDocumentForm()
    addeditdocumentform.appid.choices = [(str(a.id), a.name) for a in apps]
    if addeditdocumentform.validate_on_submit():
        if id is None:
            newdocument = Document(
                    name=addeditdocumentform.name.data,
                    link=addeditdocumentform.link.data,
                    app_id=addeditdocumentform.appid.data,
                    description=addeditdocumentform.description.data
                    ).save()
            flash("Document successfully added", "success")
        else:
            newdocument = Document.objects.get_or_404(id=id)
            app = App.objects.get_or_404(id=addeditdocumentform.appid.data)
            newdocument.update(
                    name=addeditdocumentform.name.data,
                    link=addeditdocumentform.link.data,
                    app_id=app,
                    description=addeditdocumentform.description.data
                    )
            flash("Document successfully updated", "success")
        return redirect(url_for('.manageDocuments'))
    elif id is not None:
        document = Document.objects.get_or_404(id=id)
        addeditdocumentform.appid.default = document.app_id.id
        addeditdocumentform.process()
        addeditdocumentform.name.data = document.name
        addeditdocumentform.link.data = document.link
        addeditdocumentform.description.data = document.description
    return render_template("manage/documentationnewedit.html",
                           addeditdocumentform=addeditdocumentform,
                           documentid=id)


@appcenter_blueprint.route("/manage/documentation/delete/<id>")
@login_required
@roles_required("Admin")
def manageDocumentsDelete(id):
    flash("Deleted Document", "danger")
    document = Document.objects.get_or_404(id=id)
    document.delete()
    return redirect(url_for('.manageDocuments'))


@appcenter_blueprint.route("/manage/tags")
@login_required
@roles_required("Admin")
def manageTags():
    tags = Tag.objects.all().limit(500).order_by("+name")
    return render_template("/manage/tags.html", tags=tags)


@appcenter_blueprint.route("/manage/tags/new", defaults={"id": None},
                           methods=["POST", "GET"])
@appcenter_blueprint.route("/manage/tags/<id>", methods=["POST", "GET"])
@login_required
@roles_required("Admin")
def manageTagsNewEdit(id):
    addedittagform = AddEditTagForm()
    if addedittagform.validate_on_submit():
        if id is None:
            newtag = Tag(
                    name=addedittagform.name.data,
                    description=addedittagform.description.data,
                    color=addedittagform.color.data,
                    textcolor=addedittagform.textcolor.data
                    ).save()
            flash("Tag was successfully saved", "success")
        else:
            updatetag = Tag.objects.get_or_404(id=id)
            updatetag.update(
                    name=addedittagform.name.data,
                    description=addedittagform.description.data,
                    color=addedittagform.color.data,
                    textcolor=addedittagform.textcolor.data
                    )
            flash("Tag was successfully updated", "success")
        return redirect(url_for(".manageTags"))
    elif id is not None:
        currenttag = Tag.objects.get_or_404(id=id)
        addedittagform.name.data = currenttag.name
        addedittagform.description.data = currenttag.description
        addedittagform.color.data = currenttag.color
        addedittagform.textcolor.data = currenttag.textcolor
    return render_template("manage/tagsnewedit.html", addedittagform=addedittagform, tagid=id)

@appcenter_blueprint.route("/manage/tags/delete/<id>")
@login_required
@roles_required("Admin")
def manageTagsDelete(id):
    flash("Deleted Tag", "danger")
    tag = Tag.objects.get_or_404(id=id)
    tag.delete()
    return redirect(url_for('.manageTags'))


@appcenter_blueprint.route("/manage/categories")
@login_required
@roles_required("Admin")
def manageCategories():
    categories = Category.objects.all().order_by("+weight")
    return render_template("/manage/categories.html", categories=categories)


@appcenter_blueprint.route("/manage/categories/new", defaults={"id": None},
                           methods=["POST", "GET"])
@appcenter_blueprint.route("/manage/categories/<id>", methods=["POST", "GET"])
@login_required
@roles_required("Admin")
def manageCategoriesNewEdit(id):
    addeditcategoryform = AddEditCategoryForm()
    if addeditcategoryform.validate_on_submit():
        if id is None:
            weight = Category.objects.order_by("-weight").first().weight + 1
            newcategory = Category(
                    name=addeditcategoryform.name.data,
                    description=addeditcategoryform.description.data,
                    weight=weight
                    ).save()
            flash("Category successfully added", "success")
        else:
            categoryupdate = Category.objects.get_or_404(id=id)
            categoryupdate.update(
                    name=addeditcategoryform.name.data,
                    description=addeditcategoryform.description.data
                    )
            flash("Category successfully updated", "success")
        return redirect(url_for(".manageCategories"))
    elif id is not None:
        category = Category.objects.get_or_404(id=id)
        addeditcategoryform.name.data = category.name
        addeditcategoryform.description.data = category.description
    return render_template('/manage/categoriesnewedit.html',
                           addeditcategoryform=addeditcategoryform,
                           categoryid=id)


@appcenter_blueprint.route("/manage/categories/delete/<id>")
@login_required
@roles_required("Admin")
def manageCategoriesDelete(id):
    category = Category.objects.get_or_404(id=id)
    try:
        category.delete()
    except OperationError:
        flash("Unable to delete category. Category should be empty before deletion.", "danger")
    else:
        flash("Category Successfully deleted", "success")
    return redirect(url_for('.manageCategories'))


@appcenter_blueprint.route("/manage/categories/save", methods=["POST"])
@login_required
@roles_required("Admin")
def manageCategoriesSave():
    order = request.get_json()["order"]
    response = dict(status="success", messages=[])
    categoryids = [str(c.id) for c in Category.objects.all()]
    error = False
    weight = 0
    for c in order:
        if c not in categoryids:
            response["status"] = "failure"
            response["messages"].append(
                {"action": "flash",
                 "value": "Unable to save category order."})
            error = True
            break
    if not error:
        for c in order:
            category = Category.objects.get(id=c)
            category.update(weight=weight)
            weight += 1
        response["messages"].append(
            {"action": "flash",
             "value": "Category order saved."})
    current_app.logger.debug(
        "Type:Response, Request:POST {0}, Response:{1}".format(
            url_for('.manageCategoriesSave'), str(response)))
    return json.dumps(response)
