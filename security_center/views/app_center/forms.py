from flask_wtf import FlaskForm
from flask_wtf.file import FileField
from wtforms import StringField, TextAreaField, HiddenField, SelectField
from wtforms import SelectMultipleField
from wtforms.validators import DataRequired, Optional, Length, URL


class AddEditDocumentForm(FlaskForm):
    name = StringField("Name",
                       validators=[DataRequired(),
                                   Length(max=255,
                                          message="Name too long")])
    link = StringField("Link",
                       validators=[DataRequired(),
                                   URL(message="Link must be a url")])
    description = TextAreaField("Description",
                                validators=[
                                    Length(max=4096,
                                           message="Description too long"),
                                    Optional()])
    appid = SelectField("App",
                        validators=[DataRequired(message="Please select app")])


class AddEditTagForm(FlaskForm):
    name = StringField("Name",
                       validators=[DataRequired(),
                                   Length(max=20, message="Name too long")])
    color = StringField("Background Color",
                        validators=[Optional(),
                                    Length(max=7,
                                           message="Hex color code too long")])
    textcolor = StringField("Text Color",
                            validators=[
                                Optional(),
                                Length(max=7,
                                       message="Hex color code too long")])
    description = TextAreaField("Description",
                                validators=[
                                    Length(max=4096,
                                           message="Description too long"),
                                    Optional()])


class AddEditCategoryForm(FlaskForm):
    name = StringField("Name",
                       validators=[
                           DataRequired(),
                           Length(max=50, message="Name too long")])
    description = TextAreaField("Description",
                                validators=[
                                    Length(max=4096,
                                           message="Description too long"),
                                    Optional()])


class AddEditAppForm(FlaskForm):
    name = StringField("Name",
                       validators=[
                           DataRequired(message="Name is required"),
                           Length(max=255,
                                  message="Name must be 255 characters or less")])
    description = TextAreaField("Description",
                                validators=[
                                    Optional(),
                                    Length(max=4096,
                                           message="Description too long")])
    link = StringField("Link",
                       validators=[
                           DataRequired(message="Link is required"),
                           URL(require_tld=False,
                               message="Link needs to be a URL")])
    category = SelectField("Category",
                           validators=[
                               DataRequired(message="Select a Category")])
    tags = SelectMultipleField("Tags",
                               validators=[Optional()])
    icon = FileField("Icon")


class DeleteAppForm(FlaskForm):
    appid = HiddenField("appid", validators=[DataRequired()])


class SearchForm(FlaskForm):
    search = StringField("Search")
