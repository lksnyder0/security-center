from security_center.models.mongo import mongodb


class Category(mongodb.Document):
    name = mongodb.StringField(max_length=255, required=True)
    description = mongodb.StringField(max_length=4096)
    weight = mongodb.IntField()
    meta = {"collection": "appcenter_category"}

    def __str__(self):
        return self.name


class Tag(mongodb.Document):
    name = mongodb.StringField(max_length=255, required=True)
    description = mongodb.StringField(max_length=4096)
    color = mongodb.StringField(max_length=7)
    textcolor = mongodb.StringField(max_length=7)
    meta = {"collection": "appcenter_tag"}

    @classmethod
    def find_or_create(cls, **kwargs):
        tag = cls.objects(**kwargs)
        if not tag:
            tag = cls(**kwargs).save()
        return tag

    def __str__(self):
        return self.name


class App(mongodb.Document):
    name = mongodb.StringField(max_length=255, required=True, unique=True)
    link = mongodb.StringField(max_length=1024, required=True)
    description = mongodb.StringField(max_length=4096)
    iconurl = mongodb.StringField(max_length=1024)
    iconpath = mongodb.StringField(max_length=1024)
    tags = mongodb.ListField(
        mongodb.ReferenceField('Tag',
                               reverse_delete_rule=mongodb.PULL))
    category_id = mongodb.ReferenceField(
        'Category', reverse_delete_rule=mongodb.DENY)
    meta = {"collection": "appcenter_app"}

    def __str__(self):
        return self.name


class Document(mongodb.Document):
    name = mongodb.StringField(max_length=255)
    description = mongodb.StringField(max_length=4096)
    link = mongodb.StringField(max_length=1024)
    app_id = mongodb.ReferenceField("App")
    meta = {"collection": "appcenter_document"}

    def __str__(self):
        return self.name
