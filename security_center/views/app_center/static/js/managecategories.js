$(document).ready(function() {
    sortable('#categorieslist', {
        placeholderClass: 'list-group-item'
    });
    $("#saveorder").on("click", function() {
        var url = $(this).data("url")
        var order = []
        $("#categorieslist>li").each( function(index) {
            order.push($(this).data("id"));
        });
        $.ajax({
            type: "POST",
            url: url,
            data: JSON.stringify({order: order}),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data) {
                for (var i = 0; i < data["messages"].length; i++) {
                    if ( data["messages"][i]["action"] == "flash" ) {
                        if (data["status"] == "success") {
                            var level = "success";
                        } else if (data["status"] == "failure") {
                            var level = "danger";
                        } else {
                            var level = "primary";
                        };
                        addalert(data["messages"][i]["value"], level);
                    };
                }
            }
        });
    });
});
