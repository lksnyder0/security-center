var submitAndVerifyForm = function (events, formData, url, csrftoken) {
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken)
            }
        }
    });
    $.ajax({
        url: url,
        data: formData,
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json'
    }).done(function(data) {
        if (data["status"] == "success") {
            for (var i in data["messages"]) {
                var message = data["messages"][i];
                if (message["action"] == "redirect" ) {
                    window.location.replace(message["location"]);
                };
            };
        } else {
            for ( var i in data["messages"] ) {
                var targetField = $(events.target).find("input[name=" + data["messages"][i]["name"] +"]");
                targetField.closest(".form-group").addClass("has-danger");
                targetField.tooltip({
                    delay: {
                        "show":500,
                        "hide":500,
                    },
                    placement: 'top',
                    title: data["messages"][i]["error"],
                    trigger: 'manual'
                });
                targetField.tooltip("show");
                targetField.on('input propertychange paste', function () {
                    $(this).tooltip('dispose');
                    targetField.closest(".form-group").removeClass("has-danger");
                });
            };
        };
    });
};

var processResults = function (row, tablebody, results) {
    if (results.length > 0) {
        tablebody.empty();
        $(row).show();
        for (i in results) {
            var row = "<tr>" +
                '<td>'+results[i]["name"]+'</td>' +
                '<td>'+results[i]["description"]+'</td>' +
                '<td><a href="'+results[i]["link"]+'">Open</a></td>' +
                '</tr>'
            tablebody.append(row)
        }
    } else {
        tablebody.empty();
        $(row).hide();
    }
}

$(document).ready(function() {
    $('#search').keypress(function (e) {
        if (e.which == 13) {
                var searchbox = $(this);
                var searchterms = searchbox.val();
                if (searchterms.length > 2) {
                        results = $.getJSON("/search/" + escape(searchterms), function ( data ) {
            if ( (data["apps"].length > 0) || (data["docs"].length > 0) ) {
                $("#noresultsmsg").hide();
            } else {
                $("#noresultsmsg").show();
            };

            var appstable = $("#appsresultstable").find("tbody");
            var appsrow = $("#appsresultrow");
            if (data["apps"].length > 0) {
                appstable.empty();
                appsrow.show();
                for (i in data["apps"]) {
                    var row = "<tr>" +
                        '<td>'+data["apps"][i]["name"]+'</td>' +
                        '<td>'+data["apps"][i]["description"]+'</td>' +
                        '<td><a href="'+data["apps"][i]["link"]+'" target="_blank">Open</a></td>' +
                        '</tr>';
                    appstable.append(row);
                }
            } else {
                appstable.empty();
                appsrow.hide();
            };

            var docstablebody = $("#docsresultstable").find("tbody");
            var docsrow = $("#docsresultrow");
            if (data["docs"].length > 0) {
                docstablebody.empty();
                docsrow.show();
                for (i in data["docs"]) {
                    var row = "<tr>" +
                        '<td>'+data["docs"][i]["name"]+'</td>' +
                        '<td>'+data["docs"][i]["description"]+'</td>' +
                        '<td><a href="'+data["docs"][i]["link"]+'" target="_blank">Open</a></td>' +
                        '</tr>';
                    docstablebody.append(row);
                }
            } else {
                docstablebody.empty();
                docsrow.hide();
            };
                        });
                        $('#searchresults').modal('show');
                }
                else if (searchterms.length <= 2) {
                        console.log("need terms");
                        searchbox.closest(".form-group").addClass("has-danger");
                        searchbox.tooltip({
                                delay: {
                                        "show": 500,
                                        "hide": 500
                                },
                                placement:"bottom",
                                title: "Search too short",
                                trigger: "manual"
                        });
                        searchbox.tooltip("show");
                        searchbox.on('input propertychange paste', function() {
                                searchbox.tooltip('dispose');
                                searchbox.closest(".form-group").removeClass("has-danger");
                        });
                };
                return false;
        };
    });

    $('#documentlist').on("show.bs.modal", function (event) {
        var appid = $(event.relatedTarget).data('appid');
        var modal = $(this);
        modal.find(".modal-content").empty();
        $.ajax('/documents/' + appid).done(function(html) {
            modal.find(".modal-content").append(html);
        });
        modal.find("#addeditdocuments").data("appid", appid);
    });
    $('#addeditapp').on('hide.bs.modal', function(event) {
        var modal = $(this);
        var form = $('#addeditappform')[0];
        form.reset();
        form.action = "/app/new";
        modal.find('[name="tags"]').trigger("change");
        modal.find('.has-danger').each(function () {
            $(this).removeClass('has-danger')
        })
        modal.find('input').each(function () {
            $(this).tooltip('dispose');
        });
    });
    $('#addeditappform').on('submit', function(event) {
        var action = $(this)[0].action;
        var csrftoken = $(event.target).find("input[name=csrf_token]").val();
        $(event.target).find("textarea").each(function () {
            escape($(this).val());
        });
        var formData = new FormData($(this)[0]);
        submitAndVerifyForm(event, formData, action, csrftoken);
        event.preventDefault();
        return false;
    });
    $('.editapp').on('click', function(event) {
        var appid = $(event.currentTarget).data("appid");
        $.getJSON('/app/edit/' + appid, function(data) {
            var appform = $('#addeditappform');
            appform[0].action = "/app/edit/" + appid;
            console.log(appform[0].action);
            appform.find('[name="name"]').val(data["name"]);
            appform.find('[name="link"]').val(data["link"]);
            appform.find('[name="description"]').val(data["description"]);
            appform.find('[name="category"]').val(data["category"]);
            appform.find('[name="tags"]').val(data["tags"]);
            appform.find('[name="tags"]').trigger("change");
            $('#addeditapp').modal('toggle');
        });
    });
    $('.deleteapp').on('click', function(event) {
        var appid = $(this).data('appid');
        $(this).tooltip({
            title: '<form action="/app/delete/' + appid + '" method="POST"><button type="submit" class="btn btn-xs btn-danger">Delete</button></form>',
            html: true,
            placement: 'bottom',
            trigger: "manual"
        });
        $(this).tooltip('toggle')
    });
    $("#tags").select2();
});
