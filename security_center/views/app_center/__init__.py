from flask import Blueprint

appcenter_blueprint = Blueprint('app_center', __name__,
                                template_folder="templates",
                                static_folder="static")

from .views import *
