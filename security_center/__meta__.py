# -*- coding: utf-8 -*-
# Security-Center (c) Luke Snyder

__project__ = 'Security-Center'
__version__ = '0.1.0'
__author__ = 'Luke Snyder'
__email__ = 'luke@ragriz.me'
__url__ = 'https://gitlab.com/lksnyder0/security-center'
__copyright__ = 'Luke Snyder'
