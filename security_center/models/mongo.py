from flask_mongoengine import MongoEngine
mongodb = MongoEngine()

from security_center.views.administration.models import User
from security_center.views.administration.models import Role
from security_center.views.app_center.models import Category
from security_center.views.app_center.models import Tag
from security_center.views.app_center.models import App
from security_center.views.app_center.models import Document


collections = [
        Category,
        Tag,
        App,
        Document,
        User,
        Role
        ]


