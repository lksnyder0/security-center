#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Security-Center (c) Luke Snyder

import sys
sys.path.insert(0, '.')

from flask import url_for
from flask_script import Manager, Shell, Server
from security_center import app
from security_center.models.mongo import mongodb, collections, Category, Tag
from security_center.models.mongo import App, User, Role


def _make_context():
    return {
        "app": app,
        "mongodb": mongodb
    }


manager = Manager(app)
manager.add_command("shell", Shell(make_context=_make_context))
manager.add_command("runserver", Server(port=app.config['PORT']))
manager.add_command("publicserver",
                    Server(port=app.config['PORT'], host="0.0.0.0"))


@manager.option('-e', '--email', help='email address', required=True)
@manager.option('-p', '--password', help='password', required=True)
@manager.option('-a', '--admin', help='make user an admin user',
                action='store_true', default=None)
def user_add(email, password, admin):
    "add a user to the database"
    if admin:
        roles = ["Admin"]
    else:
        roles = ["User"]
    User.register(
        email=email,
        password=password,
        confirmed=True,
        roles=roles
    )


@manager.option('-e', '--email', help='email address', required=True)
def user_del(email):
    "delete a user from the database"
    obj = User.objects.get(email=email)
    if obj:
        obj.delete()
        print("Deleted")
    else:
        print("User not found")


@manager.command
def drop_db():
    "drop all databases, instantiate schemas"
    for coll in collections:
        coll.drop_collection()


@manager.command
def populate_db():
    "insert a default set of objects"
    adminrole = Role(name="Admin", description="Admin Role").save()
    userrole = Role(name="User", description="User Role").save()
    User.add_guest_user()
    User.add_admin_user(password='password')
    otherCat = Category(name="Other", description="Uncatagorized apps",
                        weight=1000).save()
    malware = Category(name="Malware", description="Malware analysis tools",
                       weight=1).save()
    dns = Category(name="Domain", description="DNS analysis and lookup tools",
                   weight=2).save()
    internalTag = Tag(name="Internal", description="Internally hosted apps.",
                      color="#1ca001", textcolor="#000000").save()
    externalTag = Tag(name="External", description="Externally hosted apps.",
                      color="#980000", textcolor="#ffffff").save()
    app1 = App(name="Virustotal",
               description="A free service to analyze files and URLs",
               link="https://www.virustotal.com", tags=[externalTag],
               category_id=malware).save()
    app2 = App(name="Malwr", description="Public Cukoo sandbox",
               link="https://malwr.com", tags=[externalTag],
               category_id=malware).save()


@manager.command
def list_routes():
    output = []
    for rule in app.url_map.iter_rules():

        options = {}
        for arg in rule.arguments:
            options[arg] = "[{0}]".format(arg)

        methods = ','.join(rule.methods)
        url = url_for(rule.endpoint, **options)
        line = "{:50s} {:20s} {}".format(rule.endpoint, methods, url)
        output.append(line)
    for line in sorted(output):
        print(line)


if __name__ == "__main__":
    manager.run()
